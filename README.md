# Efile API 
digunakan untuk mengakses api e-file. data yang diambil adalah data pns
## Instalasi
1. clone project dengan perintah

```sh
$ git clone https://gitlab.com/khanif.zyen/efile-api.git
```
2. masuk ke folder efile-api dan lakukan composer update
```sh
$ cd efile-api
$ composer update --prefer-dist
```
3. buat database efile_efile melalui phpmyadmin
4. import efile_efile.sql melalui phpmyadmin
5. lakukan perintah init, dan pilih mode development dengan memasukkan 0
```sh
$ .\init
Yii Application Initialization Tool v1.0

Which environment do you want the application to be initialized in?

  [0] Development
  [1] Production

  Your choice [0-1, or "q" to quit] 0
  Initialize the application under 'Development' environment? [yes|no] yes

  Start initialization ...

   generate yii_test.bat
   generate yii
   generate yii_test
   generate frontend/web/robots.txt
   
   ...   
   
      chmod 0755 yii
      chmod 0755 yii_test

  ... initialization completed.
```
6. ubah konfigurasi database di common\config\main-local.php
```sh
...
'db' => [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=efile_efile',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
],
...
```
7. api bisa diakses di localhost/efile-api/api/